# python3-alpine-flask-docker 

Flask based web service for unit conversion to SI units.

Adapted from https://github.com/nikos/python3-alpine-flask-docker

## Build Docker image

Generate docker image:

```
make clean
make build
```


## Run Docker container

Spin up a container based on this docker image:

```
make run
```

Now you should be able to run  http://0.0.0.0:5088?units=(min) to see the service 
returning the SI units and conversion as a json file

Note that if you need to change the port, this currently needs to be manually changed in 3 files (app.py, Makefile, Dockerfile)

## Run the dockerized app on Heroku

It has been deployed as citrinechallenge2 in heroku. Test it by running:

https://citrinechallenge2.herokuapp.com/units/si?units=(min)

If you need to create a new app follow [these](https://medium.com/travis-on-docker/how-to-run-dockerized-apps-on-heroku-and-its-pretty-great-76e07e610e22) instructions. 

To update app:
```
heroku container:push web --app citrinechallenge2
```
