import json
import re
import math
import os.path
from exceptions import InvalidUsage
from error_codes import *

ARITHMETIC_OPS = ('+', '-', '/', '*')  # to check if the expression starts or ends with these operators
KNOWN_SYMBOLS_PATTERN = "[()A-Za-z°\"'/*+-]+"  # these are the list of acceptable symbols


class SiUnits:
    def __init__(self):
        if os.path.isfile('units.json'):
            self._data = json.load(open('units.json'))
            self._units_data = {}
            self._pre_process_data()
        else:
            raise InvalidUsage('Could not find units.json')

    @staticmethod
    def _extract_mul_factor(in_str):
        in_str = "".join(in_str.split())  # remove all whitespaces
        r = re.compile("[0-9-π()/*.]+")  # detects arithmetic expression containing numbers and pi. Good for now
        m = r.findall(in_str)
        if len(m) == 1:
            mul_str = m[0]
            r = re.compile("[0-9-()/*.]+")
            m = r.findall(in_str)
            if len(m) > 1:  # contains π. So replace it with the value of pi
                with_pi = []
                pi_str = '%.14f' % math.pi
                for m_i in m:
                    with_pi.append(m_i)
                    with_pi.append(pi_str)
                with_pi.pop()
                mul_str = "".join(with_pi)
        else:
            raise ValueError('{} in units.json is not a valid multiplication factor string.'
                             'Expecting only numbers, π ,+,-,/,*,** and paranthesis'.format(in_str))
        return '%.14f' % eval(mul_str)

    def _process_conversion_string(self, unit):
        in_str = unit['SI_Convert']
        convert_strings = in_str.split()
        if len(convert_strings) == 2:
            unit['multiplication_factor'] = self._extract_mul_factor(convert_strings[0])
            unit['unit_name'] = convert_strings[1]  # self._extract_unit_name(conv_str)
        else:
            raise ValueError('Expecting no spaces in conversion string except between '
                             'multiplication factor and si unit, but got {}'.format(in_str))

    def _extract_unit_name(self):
        raise NotImplementedError('_extract_unit_name not implemented.')
        r = re.compile("[a-zA-Z]+")
        m = r.findall(in_str)
        if len(m) == 1:
            unit_name = m[0]
        else:
            raise ValueError('{} in units.json is not a valid unit name string.\
                    Expecting only characters in upper or lower case'.format(in_str))
        return unit_name

    def _pre_process_data(self):
        for unit in self._data:
            self._process_conversion_string(unit)
            if unit['Name'] != '':  # avoiding adding empty "Name" field into _units_data dictionary
                self._units_data[unit['Name']] = {"unit_name": unit['unit_name'],
                                                  "multiplication_factor": unit["multiplication_factor"]}
            self._units_data[unit['Symbol']] = {"unit_name": unit['unit_name'],
                                                "multiplication_factor": unit["multiplication_factor"]}

    @staticmethod
    def has_unknown_symbol(inp_str):
        r = re.compile(KNOWN_SYMBOLS_PATTERN)  # now look for unit names and/or symbols
        matched_strs = r.findall(inp_str)
        matched_str = "".join(matched_strs)
        return matched_str != inp_str

    def get_si(self, in_str):
        in_str = "".join(in_str.split())  # remove all whitespaces
        if in_str.startswith(ARITHMETIC_OPS) or in_str.endswith(ARITHMETIC_OPS):
            raise InvalidUsage('Expression {} starts or ends with an arithmetic operator.'
                               'Please provide a corrected expression'.
                               format(in_str), WRONG_EXPRESSION)
        if self.has_unknown_symbol(in_str):
            num_pattern = re.compile("[0-9]+")  # search pattern for numbers. If found it will be rejected
            numbers = num_pattern.findall(in_str)
            if len(numbers) != 0:
                raise InvalidUsage('Expression {} contains numbers. Please provide expression without numbers'.
                                   format(in_str), WRONG_EXPRESSION)
            else:
                raise InvalidUsage('Expression {} contains symbols that does not match pattern {}.'
                                   'Please provide a corrected expression'.format(in_str, KNOWN_SYMBOLS_PATTERN),
                                   UNKNOWN_SYMBOL)
        out_str = in_str  # si unit name starts out as the input, but gets replaced by si names
        out_val = in_str  # si mul factor starts out as the non si names, but gets replaced by si mul factors
        r = re.compile("[A-Za-z°\"']+")  # now look for unit names and/or symbols
        non_si_units = r.findall(in_str)
        for non_si_unit in non_si_units:
            if non_si_unit in self._units_data:
                si_data = self._units_data[non_si_unit]
                # TODO: This replace approach will fail if the "Name" field is a substring of another "Name" field
                # Change this algorithm! One possible solution is to split up into words and replace whole words.
                out_str = out_str.replace(non_si_unit, si_data['unit_name'])
                out_val = out_val.replace(non_si_unit, si_data['multiplication_factor'])
            else:
                raise InvalidUsage('{} is not a known unit.'.format(non_si_unit), UNKNOWN_UNIT)
        return out_str, '%.14f' % eval(out_val)
