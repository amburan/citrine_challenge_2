import os
from flask import Flask
from flask import request
from flask import jsonify
from exceptions import InvalidUsage
from si_unit import SiUnits
from error_codes import *


app = Flask(__name__)
si_units = SiUnits()


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/units/si')
def get_si_units():
    result = {}
    non_si_unit = request.args.get('units', '')
    if non_si_unit != '':
        unit_name, mul_fact = si_units.get_si(non_si_unit)
        print(non_si_unit, ';', unit_name, ';', mul_fact)
        result['unit_name'] = unit_name
        result['multiplication_factor'] = mul_fact
    else:
        raise InvalidUsage('No unit', NO_UNITS)
    return jsonify(result)


@app.route('/')
def hello():
    return 'SI conversion services(https://bitbucket.org/amburan/citrine_challenge_2)'


if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5088.
    port = int(os.environ.get('PORT', 5088))
    app.run(host='0.0.0.0', debug=False, port=port)
